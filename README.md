We aim to create a platform that will successfully manage a portfolio of crypto currencies with minimal 
input from the user. 

This software will eventually run on our Evolution One platform, offering high speed crypto currency trading
that is plug and play.

Some of the software will be opened source. We are still deciding how we want to licence this product. Research
is 100 precent open source, to benefit the Ai and machine learning field. However, trading software will be
restricted and require a licence purchase.

Ai and deep learning has not been explored in the scale that we aim to do. This is rather suprising as the 
amount of Ai researchers and bitcoin traders is astonishing. 